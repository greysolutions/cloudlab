package it.greysolutions.corso.collections;

import org.junit.Test;

import static org.junit.Assert.*;

public class ListTest {

    @Test
    public void givenAnEmptyList_WhenAddingOneElement_IshouldGetItBack() {
        // given an empty list
        List l0 = new List();

        // when I add a new item
        l0.add("element 0");

        // I should find the element at position 0
        assertEquals(l0.get(0), "element 0");
    }

    @Test
    public void givenAnEmptyList_WhenAddingTwoElements_IshouldGetThemBack() {
        // given an empty list
        List l0 = new List();

        // when I add two items
        l0.add("element 0");
        l0.add("element 1");

        // I should find the first element at position 0
        assertEquals("element 0", l0.get(0));
        // I should find the second element at position 1
        assertEquals("element 1", l0.get(1));

    }

    @Test
    public void givenAnEmptyList_WhenItsStillEmpty_SizeShouldBeZero() {
        // given an empty list
        List l0 = new List();

        // when it's still empty

        // size should be zero
        assertEquals(0, l0.size());
    }

    @Test
    public void givenAnEmptyList_WhenIAddTwoElements_SizeShouldBeTwo() {
        // given an empty list
        List l0 = new List();

        // when I add two elements
        l0.add("element 0");
        l0.add("element 1");

        // size should be 2
        assertEquals(2, l0.size());
    }

    @Test
    public void givenAnEmptyList_ItShouldBeEmpty() {
        // given an empty list
        List l0 = new List();

        // isEmpty should be true
        assertTrue(l0.isEmpty());
    }

    @Test
    public void givenAnEmptyList_WhenIAddOneElement_ItShouldNotBeEmpty() {
        // given an empty list
        List l0 = new List();

        // when I add an element
        l0.add("element 0");

        // isEmpty should be false
        assertFalse(l0.isEmpty());
    }

    @Test
    public void givenAListWithTwoElements_WhenIRemoveOne_SizeShouldBeOne() {
        // given a list with two elements
        List l0 = new List();
        l0.add("element 0");
        l0.add("element 1");

        // when I remove one
        l0.remove("element 0");

        // size should be 1
        assertEquals(1, l0.size());
    }

    @Test
    public void givenAListWithTwoElements_WhenIRemoveTheFirst_IShouldGetTheSecond() {
        // given a list with two elements
        List l0 = new List();
        l0.add("element 0");
        l0.add("element 1");

        // when I remove one
        l0.remove("element 0");

        // I should get the second element
        assertEquals("element 1", l0.get(0));
    }

    @Test
    public void givenAListWithThreeElements_WhenIRemoveTheSecond_IShouldGetTheThird() {
        // given a list with two elements
        List l0 = new List();
        l0.add("element 0");
        l0.add("element 1");
        l0.add("element 2");

        // when I remove one
        l0.remove("element 1");

        // I should get the third element
        assertEquals("element 2", l0.get(1));
    }

}
