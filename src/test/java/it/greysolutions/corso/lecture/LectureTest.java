package it.greysolutions.corso.lecture;

import it.greysolutions.corso.Topic;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class LectureTest {

    @Test
    public void getInfo() {
        Lectures.addLecture("prova", "Firenze", new Topic("fotografia"));
        // non deve essere null
        Set<Lecture> result = Lectures.searchByName("prova");
        assertNotNull(result);
        assertTrue(result.isEmpty());
        for (Lecture l : result) {
            LectureInfo info = l.getInfo();
            if (info.name.equals("prova")) {
                assertTrue(info.location.equals("Firenze"));
                assertEquals(info.topic, new Topic("fotografia"));
            }
        }

    }

    @Test
    public void addReview() {
    }

    @Test
    public void buy() {
    }
}