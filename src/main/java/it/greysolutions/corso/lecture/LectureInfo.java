package it.greysolutions.corso.lecture;

import it.greysolutions.corso.Review;
import it.greysolutions.corso.Topic;
import it.greysolutions.corso.VideoLink;

import java.util.Set;

public final class LectureInfo {

    public LectureInfo(String name, String location, Topic topic, Set<Review> reviews, Set<VideoLink> videoLinks) {
        this.name = name;
        this.location = location;
        this.topic = topic;
        this.reviews = reviews;
        this.videoLinks = videoLinks;
    }

    public String name;
    public String location;
    public Topic topic;
    public Set<Review> reviews;
    public Set<VideoLink> videoLinks;
}
