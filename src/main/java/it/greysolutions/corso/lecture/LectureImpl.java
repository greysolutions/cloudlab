package it.greysolutions.corso.lecture;

import it.greysolutions.corso.Review;
import it.greysolutions.corso.Topic;
import it.greysolutions.corso.User;
import it.greysolutions.corso.VideoLink;

import java.util.HashSet;
import java.util.Set;

public class LectureImpl implements Lecture {

    protected String name;
    protected String location;
    protected Topic topic;
    protected Set<Review> reviews = new HashSet<>();
    protected Set<VideoLink> videoLinks = new HashSet<>();

    protected LectureImpl() {}

    @Override
    public LectureInfo getInfo() {
        return null;
    }

    @Override
    public void addReview(Review r) {

    }

    @Override
    public boolean buy(User user) {
        return false;
    }
}
