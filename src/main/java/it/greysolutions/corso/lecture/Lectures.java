package it.greysolutions.corso.lecture;

import it.greysolutions.corso.Topic;

import java.util.HashSet;
import java.util.Set;

public class Lectures {

    private Lectures() {}

    static private Set<Lecture> data = new HashSet<Lecture>();

    static Set<Lecture> searchByName(String name) {
        // TODO: implement
        return new HashSet<Lecture>();
    }

    static void addLecture(String name, String location, Topic topic) {
        LectureImpl l = new LectureImpl();
        l.name = name;
        l.location = location;
        l.topic = topic;
        data.add(l);
        // FIXME: gestire correttamente il caso in cui venga aggiunta una Lecture gia' presente
    }

}
