package it.greysolutions.corso.lecture;

import it.greysolutions.corso.Review;
import it.greysolutions.corso.User;

public interface Lecture {
    LectureInfo getInfo();
    void addReview(Review r);
    boolean buy(User user);
}
