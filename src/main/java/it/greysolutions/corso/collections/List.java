package it.greysolutions.corso.collections;

public class List {

    private Node first = new Node();
    private int size = 0;

    public void add(Object o) {
        Node current = first;

        while (current.next != null) {
            current = current.next;
        }

        current.element = o;
        current.next = new Node();
        size++;
    }

    public void remove(Object o) {
        if (size() == 0) {
            return;
        }
        if (first.element.equals(o)) {
            first = first.next;
            size--;
        }
        else {
            Node current = first.next;
            Node previous = first;

            while (current.next != null) {
                if (current.element.equals(o)) {
                    previous.next = current.next;
                    return;
                }
                current = current.next;
            }
        }

    }

    public Object get(int position) {
        if (position < 0 || position >= size) {
            return null;
        }

        Node current = first;

        for (int i = 0; i < position; i++ ) {
            current = current.next;
        }

        return current.element;

    }

    public boolean isEmpty() {
        return first.next == null;
    }

    public int size() {
        return size;
    }

    class Node {
        Node next;
        Object element;
    }
}
